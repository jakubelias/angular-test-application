'use strict';

describe('Controller: TodoCtrl', function () {

  var mockTodoService = {
    query: function() {
      return [
        { text: 'xxx', done: true },
        { text: 'yyy', done: false}
      ];
    }
  };
/*
  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));*/



  beforeEach(module('xeditable'));

  beforeEach(function () {
      //mock the xeditable lib:
      angular.module("xeditable", []);
  });

  // load the controller's module

  beforeEach(function() {
    module('xeditable');
    module('angular2App');
    module(function($provide) {
      $provide.value('todoService', mockTodoService);
    });
  });

  var TodoCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TodoCtrl = $controller('TodoCtrl', {
      $scope: scope
    });
  }));

  it('should initialize $scope', function () {
    expect(scope.todoList[0].text).toBe('xxx');
    expect(scope.todoList[1].text).toBe('yyy');
  });

  it('should set default value of property user.name', function () {
    expect(scope.user.name).toBe('1');
  });


  it('add form should be valid for default', function () {
    expect(scope.addFormValid()).toBe(true);
  });







});
