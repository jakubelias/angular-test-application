'use strict';

angular.module('angular2App')
  .controller('TodoCtrl', ['$scope', 'todoService', '$timeout', function ($scope, todoService, $timeout) {

    $scope.todoList = todoService.query();
    $scope.today = today();
    $scope.dateFormat = "dd.MM.yyyy";

    $scope.newTodo = {
      text: '',
      date: today()
    };

    $scope.user = {
      name: 'awesome user'
    };

    $scope.picker = {opened: false};

    $scope.openPicker = function () {
      $timeout(function () {
        $scope.picker.opened = true;
      });
    };

    $scope.closePicker = function () {
      $scope.picker.opened = false;
    };


    $scope.disableButtons = function () {
      $scope.isDisabled = true;
    };

    $scope.enableButtons = function () {
      $scope.isDisabled = false;
    };


    $scope.updateTodo = function (todo) {
      console.log('aftersave ' + todo.text + ', ' + todo.date);
      if (todo.text.length == 0) {
        $scope.isInvalidText = true;
        return;
      }
      if (!(typeof todo.date === "undefined" || todo.date == null)) {
          todo.date = new Date(todo.date);
      } 
      
      todoService.update(todo, function () {
      });

      $scope.todoList = todoService.query();
    };


    $scope.add = function () {
      if (!$scope.addForm.$valid) {
        return;
      }

      todoService.create($scope.newTodo, function () {
        $scope.todoList = todoService.query();
      });
      $scope.newTodo = {
        text: '',
        date: today()
      };
    }

    $scope.remove = function (todo) {

      var strconfirm = confirm("Are you sure you want to delete?");
      if (strconfirm === false) {
        return true;
      }


      todoService.remove(todo, function () {
        $scope.todoList = todoService.query();
      })
    };

    $scope.removeFinished = function () {
      todoService.removeFinished($scope.todoList, function () {
        $scope.todoList = todoService.query();
      })

    }

    $scope.done = function (todo) {
      todo.done = true;

      todoService.update(todo, function () {
        var result = todoService.query();
        $scope.todoList = result;
      })

    };


    $scope.alert = function (todo) {
      alert(todo);
    };

    $scope.onChange = function (a, b) {

    };

    function today() {
      return new Date().toLocaleDateString("en-US");
    }
   

  }]);
