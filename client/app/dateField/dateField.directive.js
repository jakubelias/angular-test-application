'use strict';

angular.module('angular2App')
  .directive('dateField', function () {
    return {
      templateUrl: 'app/dateField/dateField.html',
      restrict: 'AE',
      require: 'ngModel',
      scope: { },
      link: function (scope, element, attrs, ngModelCtrl) {
        scope.dateFormat = "M/d/yyyy";
        scope.date = '';
        scope.openPopup = openPopup;

        scope.isButtonDisabled = function() {
          return scope.isDisabled === 'true';
        };

        ngModelCtrl.$render = render;

        scope.$watch('date', onDateChanged);

        function render() {
          scope.date = ngModelCtrl.$viewValue;
        }

        function openPopup($event) {
          //console.error($event)
          $event.preventDefault();
          $event.stopPropagation();
          scope.popupOpened = true;
        }

        function onDateChanged() {
          ngModelCtrl.$setViewValue(scope.date);
        }
      }
    };
  });
