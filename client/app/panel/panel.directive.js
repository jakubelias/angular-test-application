'use strict';

angular.module('angular2App')
  .directive('panel',['$timeout', function ($timeout) {
    return {
      templateUrl: 'app/panel/panel.html',
      scope: {
        description: "@",
        todoList: "=",
        onDone: "&",
        onItemRemove: "&",
        onItemSave: "&",
        filterTagValue: "@"
      },
      restrict: 'EA',
      link: function (scope, element, attrs) {

         // bootstrap xeditable date component hack for display of popup window when is form opened
         // https://github.com/vitalets/angular-xeditable/issues/164 or https://github.com/angular-ui/bootstrap/issues/2149
        scope.picker = {opened: false};

        scope.openPicker = function () {
          $timeout(function () {
            scope.picker.opened = true;
          });
        };

        scope.closePicker = function () {
          scope.picker.opened = false;
        };
        // end of hack


      }
    };
  }]);
