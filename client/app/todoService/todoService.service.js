'use strict';


angular.module('angular2App')
  .factory('todoService', ['$resource', function ($resource) {

    var service = {
      query: query,
      create: create,
      update: update,
      remove: remove,
      removeFinished: removeFinished
    }

    var Todo = $resource('/api/todos/:id', {id: '@_id'}, {
      'query': {
        method: 'GET',
        isArray: true,
        interceptor: {response: parseResponseDates}
      }
    });

    var Clear = $resource('/api/commands/clear/:id', {id: '@_id'}, {
      'query': {
        method: 'GET',
        isArray: true,
        interceptor: {response: parseResponseDates}
      }
    });




    return service;

    function parseResponseDates(response) {

      response.resource = _.forEach(response.resource, function(todo) {
        if (typeof todo.date === "undefined" || todo.date == null) {
          todo.tag = "never";
        } else { 
          todo.date = new Date(todo.date);
          var todayEOD = new Date((new Date()).setHours(24, 0, 0, 0));
          var tomorrowEOD = new Date((new Date()).setHours(24, 0, 0, 0));
          tomorrowEOD.setDate(tomorrowEOD.getDate()+1);
          if (new Date(todo.date).getTime() < todayEOD.getTime()) {
            todo.tag = "today";
          } else  if ((new Date(todo.date).getTime() < tomorrowEOD.getTime())) {
            todo.tag = "tomorrow";
          } else {
            todo.tag = "never";
          }
        }

      });

      return response;
    }

    function query() {
      return Todo.query();
    }

    function create(todo, success, error) {
      var newTodo = new Todo(todo);
      var promise = newTodo.$save();
      promise.then(success, error);
    }

    function update(todo, success, error) {
      todo.$save(success, error);
    }

    function remove(todo, success, error) {
      todo.$remove(success, error);
    }
    
      function removeFinished(todos, success, error) {
        _.forEach(todos, function (todo) {
          if (todo.done === true) {
            todo.$remove(success, error);
          }
        });
    }

    function clear(){

    }

  }]);
