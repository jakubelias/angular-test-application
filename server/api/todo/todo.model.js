'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var TodoSchema = new Schema({
  text: String,
  done: Boolean,
  date: Date
});

module.exports = mongoose.model('Todo', TodoSchema);
